<p align="center">
     <img src="https://gitlab.com/biblioteca-autonoma-discapacidad/guia-contenido-audiovisual-bd/-/raw/main/docs/img/Logo_ColectDiscapacidad.png" width="150" />
</p>
# Biblioteca Autónoma de la Discapacidad

## Guía de contenidos audiovisuales Biblioteca Autónoma Discapacidad

Guía para indexar o agregar contenidos audiovisuales alojados fuera de la bilbioteca.

## Biblioteca

La dirección provisoria de la biblioteca es:

[http://bibliotecadiscap.ddns.net/omeka/](http://bibliotecadiscap.ddns.net/omeka/)

## Archive.org

Cuenta de Archive.org de la biblioteca es:

[@biblioteca_autonoma_discapacidad](https://archive.org/details/@biblioteca_autonoma_discapacidad)