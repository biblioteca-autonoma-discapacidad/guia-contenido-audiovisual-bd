# Audiolibro

## Consideraciones generales

Al indexar un audiolibro que se encuentra alojado fuera de la biblioteca, todas las categorías deben completarse como se hace con cualquier documento agregado. Sin embargo, en este caso, hay que considerar dos cosas:

* 1) No se debe agregar archivo, es decir, sólo se deben modificar las categorías

* 2) La categoría que requiere una edición particular es la de "**Enlace**"

Para editar adecuadamente la categoría enlace, se deben realizar los siguientes pasos:

## Audio desde Archive.org

la **Biblioteca Autónoma de la Discapacidad** cuenta con una cuenta en Archive.org, repositorio público y abierto dedicado a almacenar y preservar la producción cultural de internet.

En la cuenta [Biblioteca Autónoma de la Discapacidad](https://archive.org/details/@biblioteca_autonoma_discapacidad) se encuentran los audiolibros disponibles. Al elegir algunos de los archivos publicados, se accede a la página del audiolibro.

<p align="center">
     <img src="https://gitlab.com/biblioteca-autonoma-discapacidad/guia-contenido-audiovisual-bd/-/raw/main/docs/img/audio-archive.png" width="700" />
</p>

**En esta página, se debe guardar 3 enlaces**:

1 La url del navegador:

<p align="center">
     <img src="https://gitlab.com/biblioteca-autonoma-discapacidad/guia-contenido-audiovisual-bd/-/raw/main/docs/img/audio-url.png" width="600" />
</p>


2 El enlace para *embeber* o agregar el reproductor:


<p align="center">
     <img src="https://gitlab.com/biblioteca-autonoma-discapacidad/guia-contenido-audiovisual-bd/-/raw/main/docs/img/audio-embed-01.png" width="600" />
</p>


<p align="center">
     <img src="https://gitlab.com/biblioteca-autonoma-discapacidad/guia-contenido-audiovisual-bd/-/raw/main/docs/img/audio-embed-02.png" width="600" />
</p>


3 El enlace para descargar el audiolibro:

<p align="center">
     <img src="https://gitlab.com/biblioteca-autonoma-discapacidad/guia-contenido-audiovisual-bd/-/raw/main/docs/img/audio-descarga.png" width="600" />
</p>

Con estos 3 enlaces, ya podemos regresar a la biblioteca y editar el elemento que queremos agregar.

## Indexar audio en la biblioteca

En la categoría enlace, debemos empezar por marcar la casilla "**Use HTML**"" y luego presional en ícono "**< >**"", ambos ubicados en los recuadros desctacados de la siguiente imagen:

<p align="center">
     <img src="https://gitlab.com/biblioteca-autonoma-discapacidad/guia-contenido-audiovisual-bd/-/raw/main/docs/img/audio-enlace.png" width="600" />
</p>

Al presionar el último ícono antes mencionado, aparecerá una caja como esta:

<p align="center">
     <img src="https://gitlab.com/biblioteca-autonoma-discapacidad/guia-contenido-audiovisual-bd/-/raw/main/docs/img/audio-caja.png" width="600" />
</p>

En ella hay que pegar el código que se encuentra a continuación:

```html
<iframe width="500" height="35" src="ENLACE-EMBEBIDO-2" frameborder="0" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen"></iframe>
<br />
<a href="ENLACE-DESCARGA-2">Descarga Aquí</a><br />
<br />
<a href="ENLACE-NAVEGADOR-1">Reproducir en Archive.org</a>
```

En este código, los siguientes 3 elementos deben ser reemplazados por los enlaces antes guardados:

* **ENLACE-NAVEGADOR-1** : debe reemplazarse por la url de la página, copiado del navegador.

* **ENLACE-EMBEBIDO-2** : debe reemplazarse por el enlace para embeber.

* **ENLACE-DESCARGA-2** : debe reemplazarse por el enlace para descargar el audiolibro.

Una vez realizado esto, se ebe presionar **OK** y, si se agregaron correctamente los enlaces, ya se debe poder ver en la categoría el reproductor de archive y los enlaces:

<p align="center">
     <img src="https://gitlab.com/biblioteca-autonoma-discapacidad/guia-contenido-audiovisual-bd/-/raw/main/docs/img/audio-agregado.png" width="600" />
</p>

Una vez publicado el nuevo elemento, ya debiese ser accesible este elemento para cualquier usuario:

<p align="center">
     <img src="https://gitlab.com/biblioteca-autonoma-discapacidad/guia-contenido-audiovisual-bd/-/raw/main/docs/img/audio-publico.png" width="600" />
</p>

